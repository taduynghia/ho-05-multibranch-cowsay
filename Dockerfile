# syntax=docker/dockerfile:1
FROM node:18-alpine3.15

WORKDIR /app
EXPOSE 4000

COPY /code/package*.json /app/
COPY . .

RUN npm install && npm install express

CMD ["node", "index.js"]

